#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <sys/types.h>
#include <errno.h>
#include "plugin.h"

uint32_t VcmpPluginInit(PluginFuncs* functions, PluginCallbacks* callbacks, PluginInfo* info)
{
	info->pluginVersion = 0x1000;
	strcpy(info->name, "0.4-droproot");
	if (info->structSize == 48)
	{
		info->apiMajorVersion = PLUGIN_API_MAJOR;
		info->apiMinorVersion = PLUGIN_API_MINOR;
	}

	uint32_t retval = 1;
	if ((geteuid() == 0) || (getuid() == 0) || (getegid() == 0) || (getgid() == 0))
	{
		FILE* file = fopen("server.cfg", "r");
		if (file != NULL)
		{
			char line[512];
			char property[64];

			while (fgets(line, sizeof(line) / sizeof(line[0]), file))
			{
				sscanf(line, "%63s", property);
				property[63] = '\0';

				if (strcasecmp(property, "droproot") == 0)
				{
					retval = 0;
					printf("Dropping root permissions.\n");

					char username[64], groupname[64];
					if (sscanf(line, "%63s %63s %63[^\n]s", property, username, groupname) != 3)
					{
						printf("Error: Parameters for 'droproot' in config were not as expected. Example: droproot nobody nogroup\n");
						break;
					}

					username[63] = '\0';
					groupname[63] = '\0';

					uid_t uid, gid;
					uid = atoi(username);
					if (uid == 0)
					{
						struct passwd *user = getpwnam(username);
						if (!user)
						{
							printf("Error: User '%s' not found!\n", username);
							break;
						}
						uid = user->pw_uid;
					}

					gid = atoi(groupname);
					if (gid == 0)
					{
						struct group *group = getgrnam(groupname);
						if (!group)
						{
							printf("Error: Group '%s' not found!\n", groupname);
							break;
						}
						gid = group->gr_gid;
					}

					int u, eu, g, eg, sg;
					// Clear all the supplementary groups
					sg = setgroups(0, NULL);
					if (sg < 0)
					{
						printf("Could not remove supplementary groups! [%s]\n", strerror(errno));
						break;
					}

					// Set the group (if we are root, this sets all three group IDs)
					g = setgid(gid);
					eg = setegid(gid);
					if (g < 0 || eg < 0)
					{
						printf("Could not switch group id! [%s]\n", strerror(errno));
						break;
					}

					// and set the user (if we are root, this sets all three user IDs)
					u = setuid(uid);
					eu = seteuid(uid);
					if (u < 0 || eu < 0)
					{
						printf("Could not switch user id! [%s]\n", strerror(errno));
						break;
					}

					retval = 1;
				}
			}
			fclose(file);
		}
	}
	return retval;
}
